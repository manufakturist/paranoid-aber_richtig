

# Paranoid - aber richtig!
### Manufaktur für digitale Selbstverteidigung
![Image](images/Briefkasten_Manufaktur.svg.png)

---

## Agenda - kurz im Überblick

* Anonyme Recherche - Suchen ohne Reue
* E-Mail-Verschlüsselung - einfach, sicher

---

### Anonyme Recherche
<span style="color:gray; ;font-family:courier; font-size:1.5em">Anonyme Recherche</span>
<img src="images/Ozapft-is.jpg" alt="Ozapft is!" width="200" heigth="250">
* was bedeutet *anonyme Recherche*?
* was benötigen wir dafür?

---
